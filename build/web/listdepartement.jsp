<%-- 
    Document   : index.jsp
    Created on : Feb 6, 2015, 10:21:57 PM
    Author     : denipermana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrasi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/font.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/plugin.css">
        <!--[if lt IE 9]>
            <script src="js/ie/respond.min.js"></script>
            <script src="js/ie/html5.js"></script>
            <script src="js/ie/excanvas.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- header -->
	<header id="header" class="navbar">
            <a class="navbar-brand" href="#">UBL</a>
            <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
              <i class="fa fa-bars fa-lg text-default"></i>
            </button>
        </header>
        <!-- / header -->
        <%@include file="include/nav.jsp" %>
        <section id="content">
            <section class="main padder">
                <div class="row">
                    <div class="col-lg-12">
                        <h4>Staff</h4>
                        <hr />
                        <% if(session.getAttribute("success") != null){ %>
                            <div class="alert alert-success"><%= session.getAttribute("success") %></div>
                            <% session.removeAttribute("success"); %>
                        <% } %>
                        <section class="panel">
                            <header class="panel-heading">
                                <form action="UserController" method="POST">
                                <div class="input-group">
                                    <input type="text" class="input-sm form-control" name="searchparam" placeholder="Cari">
                                    <span class="input-group-btn">
                                        <input type="submit" name="btn" value="Search" class="btn btn-sm btn-white" />
                                    </span>
                                </div>
                                </form>
                            </header>
                        <div>

                          <table class="table table-striped m-b-none text-small">
                            <thead>
                              <tr>
                                <th>Nama</th>
                                <th width="300">Action</th>
                              </tr>
                            </thead>
                            <tbody>        

                                <c:forEach items="${deps}" var="dep">
                                    <tr>
                                        <td><c:out value="${dep.name}" /></td>
                                        <td>
                                            <a class="btn btn-xs btn-warning" href="DepartementController?action=edit&depId=<c:out value="${dep.id}"/>">Update</a>
                                            <a class="btn btn-xs btn-danger" href="DepartementController?action=delete&depId=<c:out value="${dep.id}"/>">Delete</a>
                                        </td>
                                    </tr>
                                </c:forEach>

                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <a href="DepartementController?action=add" class="btn btn-default btn-xs">Tambah</a>
                    </div>
                    </section>
                </div>
                </div>
            </section>
        </section>
    </body>
</html>
