<%-- 
    Document   : index.jsp
    Created on : Feb 6, 2015, 10:21:57 PM
    Author     : denipermana
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrasi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/font.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/plugin.css">
        <!--[if lt IE 9]>
            <script src="js/ie/respond.min.js"></script>
            <script src="js/ie/html5.js"></script>
            <script src="js/ie/excanvas.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- header -->
	<header id="header" class="navbar">
            <a class="navbar-brand" href="#">UBL</a>
            <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
              <i class="fa fa-bars fa-lg text-default"></i>
            </button>
        </header>
        <!-- / header -->
        <%@include file="include/nav.jsp" %>
        <section id="content">
            <section class="main padder">
                <div class="row">
                    <div class="col-lg-12">
                        <br /><br />
                            <section class="panel">
                                <header class="panel-heading">Departement</header>
                                <div class="panel-body">
                                    <% String action = request.getParameter("action");
                                        System.out.println(action);
                                    %>
                                    <form action="DepartementController" method="POST">
                                         <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="name" value="<c:out value="${dep.name}" />" placeholder="Nama Departement" class="form-control">
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Nama Departement</label>
                                            <input type="text" name="name" placeholder="Nama Departement" class="form-control">
                                        </div>
                                        <% } %>
                                        <input type="hidden" name="depid" value="<c:out value="${dep.id}" />">
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <input type="submit" value="Update" name="btn" class="btn btn-primary btn-block" />
                                        <% } else {%>
                                        <input type="submit" value="Tambah" name="btn" class="btn btn-primary btn-block" />
                                        <% } %>
                                    </form>
                                </div>
                            </section>
                    </div>
                </div>
            </section>
        </section>
                    
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.js"></script>
        <!-- app -->
        <script src="assets/js/app.js"></script>
        <script src="assets/js/app.plugin.js"></script>
        <script src="assets/js/app.data.js"></script>
        <!-- fuelux -->
        <script src="assets/js/fuelux/fuelux.js"></script>
        <!-- datepicker -->
        <script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
        <!-- slider -->
        <script src="assets/js/slider/bootstrap-slider.js"></script>
        <!-- file input -->  
        <script src="assets/js/file-input/bootstrap.file-input.js"></script>
        <!-- combodate -->
        <script src="assets/js/combodate/moment.min.js"></script>
        <script src="assets/js/combodate/combodate.js"></script>
        <!-- parsley -->
        <script src="assets/js/parsley/parsley.min.js"></script>
        <!-- select2 -->
        <script src="assets/js/select2/select2.min.js"></script>
        
    </body>
</html>
