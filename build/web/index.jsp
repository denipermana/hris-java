<%-- 
    Document   : index
    Created on : Feb 13, 2015, 11:39:19 PM
    Author     : denipermana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/font.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/plugin.css">
        <!--[if lt IE 9]>
            <script src="js/ie/respond.min.js"></script>
            <script src="js/ie/html5.js"></script>
            <script src="js/ie/excanvas.js"></script>
        <![endif]-->
    </head>
    <body>
        <% if(session.getAttribute("userid") == null || session.getAttribute("userid") == "") {  %>
        <!-- header -->
        <header id="header" class="navbar bg bg-black">
            &nbsp;
        </header>
        <!-- / header -->
        <section id="content">
          <div class="main padder">
            <div class="row">
              <div class="col-lg-4 col-lg-offset-4 m-t-large">
                <section class="panel">
                  <header class="panel-heading text-center">
                    Sign in
                  </header>
                  <form action="UserController" method="post" class="panel-body">
                    <div class="block">
                      <label class="control-label">Username</label>
                      <input type="text" placeholder="Username" name="username" class="form-control">
                    </div>
                    <div class="block">
                      <label class="control-label">Password</label>
                      <input type="password" id="inputPassword" placeholder="Password" name="password" class="form-control">
                    </div>
                    <% if(session.getAttribute("error") != null || session.getAttribute("error") == "") { %>
                        <div class="alert alert-danger"><%= session.getAttribute("error") %></div>
                        <% session.removeAttribute("error"); %>
                    <% } %>
                    <input type="submit" class="btn btn-info btn-block" value="Login" name="btn">
                  </form>
                </section>
              </div>
            </div>
          </div>
        </section>
        <!-- footer -->
        <footer id="footer">
          <div class="text-center padder clearfix">
            <p>
              <small>&copy; Form Registrasi Karyawan</small><br><br>
            </p>
          </div>
        </footer>
        <% } else {%>
            <jsp:forward page="/UserController?action=listuser" />
        <% } %>
        <!-- / footer -->
            
        
        <!-- / footer -->
    </body>
</html>
