<%-- 
    Document   : index.jsp
    Created on : Feb 6, 2015, 10:21:57 PM
    Author     : denipermana
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrasi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/font.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/plugin.css">
        <!--[if lt IE 9]>
            <script src="js/ie/respond.min.js"></script>
            <script src="js/ie/html5.js"></script>
            <script src="js/ie/excanvas.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- header -->
	<header id="header" class="navbar">
            <a class="navbar-brand" href="#">UBL</a>
            <button type="button" class="btn btn-link pull-left nav-toggle visible-xs" data-toggle="class:slide-nav slide-nav-left" data-target="body">
              <i class="fa fa-bars fa-lg text-default"></i>
            </button>
        </header>
        <!-- / header -->
        <%@include file="include/nav.jsp" %>
        <section id="content">
            <section class="main padder">
                <div class="row">
                    <div class="col-lg-12">
                        <br /><br />
                            <section class="panel">
                                <header class="panel-heading">Data Karyawan</header>
                                <div class="panel-body">
                                    <% String action = request.getParameter("action");
                                        System.out.println(action);
                                    %>
                                    <form action="UserController" method="POST">
                                         <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" value="<c:out value="${user.username}" />" placeholder="Username" class="form-control" readonly="readonly">
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" placeholder="Username" class="form-control">
                                        </div>
                                        <% } %>
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" value="<c:out value="${user.email}" />"  placeholder="Email" class="form-control">
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Email</label>
                                                <input type="text" name="email"  placeholder="Email" class="form-control">
                                            </div>
                                        <% } %>
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Password</label>
                                            
                                            <input type="password"  value="<c:out value="${user.password}" />"  name="password" placeholder="Password" class="form-control">
                                        </div>
                                            <input type="hidden" value="<c:out value="${user.id}" />" name="userId" />
                                        <% } else { %>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" name="password" placeholder="Password" class="form-control">
                                            </div>
                                        <% } %>
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" name="nama" value="<c:out value="${user.nama}" />"  placeholder="Nama Lengkap" class="form-control">
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Nama Lengkap</label>
                                                <input type="text" name="nama"  placeholder="Nama Lengkap" class="form-control">
                                            </div>
                                        <% } %>
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea name="nama" placeholder="Nama Lengkap" class="form-control"><c:out value="${user.nama}" /></textarea>
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Alamat</label>
                                                <textarea name="nama" placeholder="Alamat" class="form-control"></textarea>
                                            </div>
                                        <% } %>
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <div class="form-group">
                                            <label>Departement</label>
                                            <input type="text" name="departement" value="<c:out value="${user.nama}" />"  placeholder="Departement" class="form-control">
                                        </div>
                                        <% } else { %>
                                            <div class="form-group">
                                            <label>Departement</label>
                                                <input type="text" name="departement"  placeholder="Departement" class="form-control">
                                            </div>
                                        <% } %>
                                        
                                        <% if (action.equalsIgnoreCase("edit")) {%>
                                        <input type="submit" value="Update" name="btn" class="btn btn-primary btn-block" />
                                        <% } else {%>
                                        <input type="submit" value="Tambah" name="btn" class="btn btn-primary btn-block" />
                                        <% } %>
                                    </form>
                                </div>
                            </section>
                    </div>
                </div>
            </section>
        </section>
                    
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.js"></script>
        <!-- app -->
        <script src="assets/js/app.js"></script>
        <script src="assets/js/app.plugin.js"></script>
        <script src="assets/js/app.data.js"></script>
        <!-- fuelux -->
        <script src="assets/js/fuelux/fuelux.js"></script>
        <!-- datepicker -->
        <script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
        <!-- slider -->
        <script src="assets/js/slider/bootstrap-slider.js"></script>
        <!-- file input -->  
        <script src="assets/js/file-input/bootstrap.file-input.js"></script>
        <!-- combodate -->
        <script src="assets/js/combodate/moment.min.js"></script>
        <script src="assets/js/combodate/combodate.js"></script>
        <!-- parsley -->
        <script src="assets/js/parsley/parsley.min.js"></script>
        <!-- select2 -->
        <script src="assets/js/select2/select2.min.js"></script>
        
    </body>
</html>
