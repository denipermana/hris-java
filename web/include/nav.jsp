<%-- 
    Document   : nav
    Created on : Feb 14, 2015, 9:39:32 AM
    Author     : denipermana
--%>

<nav id="nav" class="nav-primary hidden-xs">
    <ul class="nav" data-spy="affix" data-offset-top="50">
      <li class="active"><a href="UserController?action=listuser"><i class="fa fa-dashboard fa-lg"></i><span>Dashboard</span></a></li>
      <li class=""><a href="UserController?action=listuser"><i class="fa fa-dashboard fa-lg"></i><span>Staff</span></a></li>
      <li class=""><a href="BiodataController?action=listbiodata"><i class="fa fa-dashboard fa-lg"></i><span>Biodata</span></a></li>
      <li class=""><a href="GroupController?action=listgroup"><i class="fa fa-dashboard fa-lg"></i><span>User Group</span></a></li>
      <li class=""><a href="DepartementController?action=listdep"><i class="fa fa-dashboard fa-lg"></i><span>Departement</span></a></li>
      <li class=""><a href="PelamarController?action=listpelamar"><i class="fa fa-dashboard fa-lg"></i><span>Pelamar</span></a></li>
      <li class=""><a href="UserController?action=logout"><i class="fa fa-dashboard fa-lg"></i><span>Logout</span></a></li>
    </ul>
</nav>