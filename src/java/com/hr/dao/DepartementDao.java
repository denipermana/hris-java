/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.dao;

import com.hr.model.User;
import com.hr.util.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hr.model.Departement;
import com.hr.model.Departement;
/**
 *
 * @author denipermana
 */
public class DepartementDao {
    private Connection connection;

    public DepartementDao() {
        connection = Database.getConnection();
    }
    
    public void addDepartement(Departement departement) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into departement(name) values (?)");
            // Parameters start with 1
            preparedStatement.setString(1, departement.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public Departement getDepartementById(String id) {
        Departement departement = new Departement();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from departement where id=?");
            preparedStatement.setString(1, id);
            ResultSet rs = preparedStatement.executeQuery();
 
            if (rs.next()) {
                departement.setId(rs.getString("id"));
                departement.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
 
        return departement;
        
    }
    public void deleteDepartement(String depid) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from departement where id=?");
            preparedStatement.setString(1, depid);
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void updateDepartement(Departement departement){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("update departement set name=? where id=?");
            preparedStatement.setString(1, departement.getName());
            preparedStatement.setString(2, departement.getId());
            preparedStatement.executeUpdate();
            System.out.println(preparedStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public List<Departement> getAllDepartement() {
        List<Departement> departements = new ArrayList<Departement>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from departement");
            while (rs.next()) {
                Departement departement = new Departement();
                departement.setId(rs.getString("id"));
                departement.setName(rs.getString("name"));
                departements.add(departement);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return departements;
    }
    
   
}
