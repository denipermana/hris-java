/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.controller;

import com.hr.dao.DepartementDao;
import com.hr.model.Departement;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author denipermana
 */
public class DepartementController extends HttpServlet{
    private static String INSERT_OR_EDIT = "/departement.jsp";
    private static String LIST = "/listdepartement.jsp";
    private DepartementDao dao;
    HttpSession session;
    public DepartementController(){
        super();
        dao = new DepartementDao();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        String forward="";
        if(session.getAttribute("userid") == null || session.getAttribute("userid") == "") {
            forward = "index.jsp";
        } else {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("delete")){
                String depId = request.getParameter("depId");
                dao.deleteDepartement(depId);
                forward = LIST;
                request.setAttribute("deps", dao.getAllDepartement());  
            } else if (action.equalsIgnoreCase("add")){
                forward = INSERT_OR_EDIT;
            } else if (action.equalsIgnoreCase("edit")){
                forward = INSERT_OR_EDIT;
                String depId = request.getParameter("depId");
                Departement dep = dao.getDepartementById(depId);
                request.setAttribute("dep", dep);
            } else if (action.equalsIgnoreCase("listdep")){
                forward = LIST;

                request.setAttribute("deps", dao.getAllDepartement());
            } else {
                forward = INSERT_OR_EDIT;

            }
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String post = request.getParameter("btn");
        if(post.equalsIgnoreCase("search")){
            RequestDispatcher view = request.getRequestDispatcher(LIST);
//            request.setAttribute("departements", dao.getUserByUsername(request.getParameter("searchparam")));
            view.forward(request, response);
        } else if(post.equalsIgnoreCase("Tambah")) {
            Departement dep = new Departement();
            dep.setName(request.getParameter("name"));
            dao.addDepartement(dep);
            RequestDispatcher view = request.getRequestDispatcher(LIST);
            session = request.getSession();
            session.setAttribute("success", "Berhasil tambah departement");
            request.setAttribute("deps", dao.getAllDepartement());
            view.forward(request, response);
        } else if(post.equalsIgnoreCase("update")) {
            Departement dep = new Departement();
            session = request.getSession();
            dep.setName(request.getParameter("name"));
            dep.setId(request.getParameter("depid"));
            System.out.println(dep);
            dao.updateDepartement(dep);
            RequestDispatcher view = request.getRequestDispatcher(LIST);
            session = request.getSession();
            session.setAttribute("success", "Berhasil update departement");
            request.setAttribute("deps", dao.getAllDepartement());
            view.forward(request, response);
        }
    }
}
