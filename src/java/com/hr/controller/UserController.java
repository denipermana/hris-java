/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hr.dao.UserDao;
import com.hr.model.User;
import javax.servlet.http.HttpSession;
/**
 *
 * @author denipermana
 */
public class UserController extends HttpServlet{
    private static String INSERT_OR_EDIT = "/users.jsp";
    private static String LIST_USER = "/listuser.jsp";
    private UserDao dao;
    HttpSession session;
    
    public UserController(){
        super();
        dao = new UserDao();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        session = request.getSession();
        
        String forward="";
        if(session.getAttribute("userid") == null || session.getAttribute("userid") == "") {
            forward = "index.jsp";
        } else {
            String action = request.getParameter("action");
            if (action.equalsIgnoreCase("delete")){
                session = request.getSession();
                String userId = request.getParameter("userId");
                dao.deleteUser(userId);
                session.setAttribute("success", "Berhasil delete user");
                forward = LIST_USER;
                request.setAttribute("users", dao.getAllUsers());  
            } else if (action.equalsIgnoreCase("add")){
                forward = INSERT_OR_EDIT;
            } else if (action.equalsIgnoreCase("edit")){
                forward = INSERT_OR_EDIT;
                String userId = request.getParameter("userId");
                User user = dao.getUserById(userId);
                request.setAttribute("user", user);
            } else if (action.equalsIgnoreCase("listUser")){
                forward = LIST_USER;
                request.setAttribute("users", dao.getAllUsers());
            } else if (action.equalsIgnoreCase("logout")){
                session = request.getSession();
                session.removeAttribute("userid");
                
            } else {
                forward = INSERT_OR_EDIT;

            }
        }
 
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String post = request.getParameter("btn");
        if(post.equalsIgnoreCase("search")){
            RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
            request.setAttribute("users", dao.getUserByUsername(request.getParameter("searchparam")));
            view.forward(request, response);
        } else if(post.equalsIgnoreCase("Tambah")) {
            User user = new User();
            user.setUsername(request.getParameter("username"));
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            dao.addUser(user);
            RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
            session = request.getSession();
            session.setAttribute("success", "Berhasil tambah user");
            request.setAttribute("users", dao.getAllUsers());
            view.forward(request, response);
        } else if (post.equalsIgnoreCase("login")){
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            RequestDispatcher view;
            session = request.getSession();
            if(username == null || username == "") {
                    session = request.getSession();
                    session.setAttribute("error", "username cannot be empty");
                    view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);
            } else if(password == null || password == "") {
                    session = request.getSession();
                    session.setAttribute("error", "password cannot be empty");
                    view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);
            } else if(username != null || username != "" ){
                boolean valid = dao.login(username, password);
                if(valid){
                    User user = dao.getUserByUsername(username);
                    System.out.println(user);
                    session.setAttribute("userid", user.getId());
                    request.setAttribute("users", dao.getAllUsers());
                    view = request.getRequestDispatcher(LIST_USER);
                    view.forward(request, response);
                } else {
                    session = request.getSession();
                    session.setAttribute("error", "User not found");
                    view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);
                }
            } 
        } else if(post.equalsIgnoreCase("update")) {
            User user = new User();
            session = request.getSession();
            session.setAttribute("success", "Berhasil updateuser");
            user.setUsername(request.getParameter("username"));
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            user.setId(request.getParameter("userId"));
            dao.updateUser(user);
            RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
            session = request.getSession();
            session.setAttribute("success", "Berhasil update user");
            request.setAttribute("users", dao.getAllUsers());
            view.forward(request, response);
        }
    }
}
