/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.model;


/**
 *
 * @author denipermana
 */
public class Departement {
    private String name;
    private String id;
    
    public String getId() {
        return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    @Override
    public String toString(){
        return "Departement [id="+id+", name="+name+"]";
    }
    
}
