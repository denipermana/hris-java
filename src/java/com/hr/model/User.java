/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.model;

import java.util.Date;

/**
 *
 * @author denipermana
 */
public class User {
    private String id;
    private String username, password, email, nama, alamat, departement, jabatan;
    private Date tgl_lahir;
    private boolean valid;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    } 
    public void setUsername(String username){
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    } 
    public void setPassword(String password){
        this.password = password;
    }
    public String getEmail() {
        return email;
    } 
    public void setEmail(String email){
        this.email = email;
    }
    public String getNama(){
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getAlamat(String alamat){
        return alamat;
    }
    public String getDepartement() {
        return departement;
    }
    public void setDepartement(String departement){
        this.departement = departement;
    }
    
    public String getJabatan() {
        return jabatan;
    }
    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    
    public Date getDate(){
        return tgl_lahir;
    }
    public void setDate() {
        this.tgl_lahir = tgl_lahir;
    }
    
    public boolean getValid() {
        return valid;
    }
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    @Override
    public String toString(){
        return "User [id="+id+", username="+username+", password="+password+", email="+email+"]";
    }
    
}
