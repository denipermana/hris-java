/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hr.model;

import java.util.Date;

/**
 *
 * @author denipermana
 */
public class Biodata {
    private String nama, alamat, tempat_lahir, departement, jabatan;
    private Date tgl_lahir;
    
    public String getNama() {
        return nama;
    }
    public void setNama(String nama)  {
        this.nama = nama;
    }
    
    public String getalamat(){
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    public String getTempatLahir() {
        return tempat_lahir;
    }
    public void setTempatLahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }
    public String getDepartement() {
        return departement;
    }
    public void setDepartement(String departement) {
        this.departement = departement;
    }
    public String getJabatan() {
        return jabatan;
    }
    
    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
    
    public Date setTglLahir() {
        return tgl_lahir;
    }
    public void setTglLahir(Date tgl_lahir){
        this.tgl_lahir = tgl_lahir;
    }
    
    
}
